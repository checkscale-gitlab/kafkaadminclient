using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using Xunit.Abstractions;

using KafkaAdmin.WebApp;


namespace KafkaAdmin.Kafka.IntegrationTests
{
    public class ApiControllerTest
    {
        [Fact]
        public void ApiControllerTest_Ping_Returns_Pong()
        {
            var appFactory = new WebApplicationFactory<WebApp.Startup>();

            /** THIS CODE HANGS FOR ADMIN CLIENT **/
            using (var client = appFactory.CreateClient())
            {
                Console.WriteLine("WE ARE IN THE TEST HERE");
            }
        }
    }
}

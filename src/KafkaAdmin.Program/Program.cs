﻿using System;
using System.Threading.Tasks;

using Confluent.Kafka;
using Confluent.Kafka.Admin;


namespace KafkaAdmin.Program
{
    class Program
    {
        public string BootStrapServers { get; }
        public short Partitions { get; }
        public short Replicas { get; }
        public string Topic { get; }


        Program(string bootStrapServers, string topic, short replicas, short partitions)
        {
            BootStrapServers = bootStrapServers;
            Partitions = partitions;
            Replicas = replicas;
            Topic = topic;
        }

        private IAdminClient CreateAdminClient()
        {
            var adminClientBuilder = new AdminClientBuilder(
                                    new AdminClientConfig()
                                    {
                                        BootstrapServers = this.BootStrapServers
                                    }
                            );
            return adminClientBuilder.Build();
        }

        public async Task CreateTopicAsync(IAdminClient client)
        {
            try
            {
                Console.WriteLine("Admin service trying to create Kafka Topic...");
                Console.WriteLine($"Topic::{Topic}, ReplicationCount::{Replicas}, PartitionCount::{Partitions}");
                Console.WriteLine($"Bootstrap Servers::{BootStrapServers}");

                await client.CreateTopicsAsync(new TopicSpecification[] {
                        new TopicSpecification {
                            Name = Topic,
                            NumPartitions = 3,
                            ReplicationFactor = 1
                        }
                    }, null);

                Console.WriteLine($"Admin service successfully created topic {Topic}");
            }
            catch (CreateTopicsException e)
            {
                if (e.Results[0].Error.Code != ErrorCode.TopicAlreadyExists)
                {
                    Console.WriteLine($"An error occured creating topic {Topic}: {e.Results[0].Error.Reason}");
                    throw e;
                }
                else
                {
                    Console.WriteLine($"Topic {Topic} already exists");
                }
            }
        }


        static async Task Main(string[] args)
        {
            const string bootStrapServers = "kafka:9092";
            const string topic = ".NetClientTopic";
            const short replicas = 1;
            const short partitions = 1;

            Console.WriteLine("Confluent .NET Client Kafka Test Program");

            var program = new Program(bootStrapServers, topic, replicas, partitions);
            using (var client = program.CreateAdminClient())
                await program.CreateTopicAsync(client);

            Console.WriteLine($".NET Client successfully created topic {topic}");
        }
    }
}

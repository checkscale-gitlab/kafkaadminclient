﻿using Autofac;
using Confluent.Kafka;

using KafkaAdmin.Kafka;
using KafkaAdmin.Kafka.Config;


namespace KafkaAdmin.Kafka.AutofacModule
{
    public class KafkaModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // registry a factory method for creating a Kafka admin client instance
            builder.Register<KafkaAdminFactory>(context =>
            {
                return (KafkaConfig config) =>
                {
                    var adminClientBuilder = new AdminClientBuilder(
                            new AdminClientConfig()
                            {
                                BootstrapServers = config.Consumer.BootstrapServers
                            }
                    );
                    return adminClientBuilder.Build();
                };

            }).SingleInstance();
        }
    }
}
